// Copyright 2010 Google Inc. All rights reserved.

package com.google.appengine.api.datastore;

import static com.google.appengine.api.datastore.DatastoreApiHelper.makeAsyncCall;
import static com.google.appengine.api.datastore.DatastoreAttributes.DatastoreType.MASTER_SLAVE;
import static com.google.appengine.api.datastore.FutureHelper.quietGet;
import static com.google.appengine.api.datastore.ImplicitTransactionManagementPolicy.AUTO;
import static com.google.appengine.api.datastore.ReadPolicy.Consistency.EVENTUAL;

import com.google.appengine.api.datastore.Batcher.IndexedItem;
import com.google.appengine.api.datastore.Batcher.ReorderingMultiFuture;
import com.google.appengine.api.datastore.DatastoreAttributes.DatastoreType;
import com.google.appengine.api.datastore.EntityCachingStrategy.PreGetCachingResult;
import com.google.appengine.api.datastore.EntityCachingStrategy.PreMutationCachingResult;
import com.google.appengine.api.datastore.FutureHelper.MultiFuture;
import com.google.appengine.api.datastore.Index.IndexState;
import com.google.appengine.api.utils.FutureWrapper;
import com.google.apphosting.api.ApiBasePb.StringProto;
import com.google.apphosting.datastore.DatastoreV3Pb.AllocateIdsRequest;
import com.google.apphosting.datastore.DatastoreV3Pb.AllocateIdsResponse;
import com.google.apphosting.datastore.DatastoreV3Pb.CompositeIndices;
import com.google.apphosting.datastore.DatastoreV3Pb.DeleteRequest;
import com.google.apphosting.datastore.DatastoreV3Pb.DeleteResponse;
import com.google.apphosting.datastore.DatastoreV3Pb.GetRequest;
import com.google.apphosting.datastore.DatastoreV3Pb.GetResponse;
import com.google.apphosting.datastore.DatastoreV3Pb.PutRequest;
import com.google.apphosting.datastore.DatastoreV3Pb.PutResponse;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.io.protocol.ProtocolMessage;
import com.google.storage.onestore.v3.OnestoreEntity.CompositeIndex;
import com.google.storage.onestore.v3.OnestoreEntity.EntityProto;
import com.google.storage.onestore.v3.OnestoreEntity.Reference;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;

/**
 * Implements AsyncDatastoreService by making calls to ApiProxy.
 *
 */
class AsyncDatastoreServiceImpl extends BaseDatastoreServiceImpl
    implements AsyncDatastoreService, CurrentTransactionProvider {

  /**
   * A base batcher for DatastoreV3 operations executed in the context of an {@link
   * AsyncDatastoreServiceImpl}.
   * @param <S> the response message type
   * @param <R> the request message type
   * @param <F> the Java specific representation of a value
   * @param <T> the proto representation of value
   */
  private abstract class V3Batcher<S extends ProtocolMessage<S>, R extends ProtocolMessage<R>,
      F, T extends ProtocolMessage<T>> extends Batcher<R, F, T> {
    protected abstract Future<S> makeCall(R batch);

    @Override
    final R newBatch(R baseBatch) {
      return baseBatch.clone();
    }

    @Override
    final int getMaxSize() {
      return datastoreServiceConfig.maxRpcSizeBytes;
    }

    @Override
    final int getMaxGroups() {
      return datastoreServiceConfig.maxEntityGroupsPerRpc;
    }

    final List<Future<S>> makeCalls(Iterator<R> batches) {
      List<Future<S>> futures = new ArrayList<Future<S>>();
      while (batches.hasNext()) {
        futures.add(makeCall(batches.next()));
      }
      return futures;
    }
  }

  /**
   * A base batcher for operations that operate on {@link Key}s.
   * @param <S> the response message type
   * @param <R> the request message type
   */
  private abstract class V3KeyBatcher<S extends ProtocolMessage<S>, R extends ProtocolMessage<R>>
      extends V3Batcher<S, R, Key, Reference> {
    @Override
    final Object getGroup(Key value) {
      return value.getRootKey();
    }

    @Override
    final Reference toPb(Key value) {
      return KeyTranslator.convertToPb(value);
    }
  }

  private final V3KeyBatcher<DeleteResponse, DeleteRequest> deleteBatcher =
      new V3KeyBatcher<DeleteResponse, DeleteRequest>() {
        @Override
        void addToBatch(Reference value, DeleteRequest batch) {
          batch.addKey(value);
        }

        @Override
        int getMaxCount() {
          return datastoreServiceConfig.maxBatchWriteEntities;
        }

        @Override
        protected Future<DeleteResponse> makeCall(DeleteRequest batch) {
          return makeAsyncCall(apiConfig, "Delete", batch, new DeleteResponse());
        }
      };

  private final V3KeyBatcher<GetResponse, GetRequest> getByKeyBatcher =
      new V3KeyBatcher<GetResponse, GetRequest>() {
        @Override
        void addToBatch(Reference value, GetRequest batch) {
          batch.addKey(value);
        }

        @Override
        int getMaxCount() {
          return datastoreServiceConfig.maxBatchReadEntities;
        }

        @Override
        protected Future<GetResponse> makeCall(GetRequest batch) {
          return makeAsyncCall(apiConfig, "Get", batch, new GetResponse());
        }
      };

  private final V3Batcher<GetResponse, GetRequest, Reference, Reference> getByReferenceBatcher =
      new V3Batcher<GetResponse, GetRequest, Reference, Reference>() {
        @Override
        final Object getGroup(Reference value) {
          return value.getPath().getElement(0);
        }

        @Override
        final Reference toPb(Reference value) {
          return value;
        }

        @Override
        void addToBatch(Reference value, GetRequest batch) {
          batch.addKey(value);
        }

        @Override
        int getMaxCount() {
          return datastoreServiceConfig.maxBatchReadEntities;
        }

        @Override
        protected Future<GetResponse> makeCall(GetRequest batch) {
          return makeAsyncCall(apiConfig, "Get", batch, new GetResponse());
        }
      };

  private final V3Batcher<PutResponse, PutRequest, Entity, EntityProto> putBatcher =
      new V3Batcher<PutResponse, PutRequest, Entity, EntityProto>() {
        @Override
        Object getGroup(Entity value) {
          return value.getKey().getRootKey();
        }

        @Override
        void addToBatch(EntityProto value, PutRequest batch) {
          batch.addEntity(value);
        }

        @Override
        int getMaxCount() {
          return datastoreServiceConfig.maxBatchWriteEntities;
        }

        @Override
        protected Future<PutResponse> makeCall(PutRequest batch) {
          return makeAsyncCall(apiConfig, "Put", batch, new PutResponse());
        }

        @Override
        EntityProto toPb(Entity value) {
          return EntityTranslator.convertToPb(value);
        }
      };

  private DatastoreType datastoreType;

  public AsyncDatastoreServiceImpl(
      DatastoreServiceConfig datastoreServiceConfig, TransactionStack defaultTxnProvider) {
    super(validateDatastoreServiceConfig(datastoreServiceConfig), defaultTxnProvider);
  }

  /**
   * @param datastoreServiceConfig Config to validate.
   * @return The config that was passed in as a parameter.
   */
  private static DatastoreServiceConfig validateDatastoreServiceConfig(
      DatastoreServiceConfig datastoreServiceConfig) {
    if (datastoreServiceConfig.getImplicitTransactionManagementPolicy() == AUTO) {
      throw new IllegalArgumentException("The async datastore service does not support an "
          + "implicit transaction management policy of AUTO");
    }
    return datastoreServiceConfig;
  }

  @Override
  public Future<Entity> get(Key key) {
    GetOrCreateTransactionResult result = getOrCreateTransaction();
    return get(result.getTransaction(), key);
  }

  @Override
  public Future<Entity> get( Transaction txn, final Key key) {
    if (key == null) {
      throw new NullPointerException("key cannot be null");
    }
    Future<Map<Key, Entity>> entities = get(txn, Arrays.asList(key));
    return new FutureWrapper<Map<Key, Entity>, Entity>(entities) {
      @Override
      protected Entity wrap(Map<Key, Entity> entities) throws Exception {
        Entity entity = entities.get(key);
        if (entity == null) {
          throw new EntityNotFoundException(key);
        }
        return entity;
      }

      @Override
      protected Throwable convertException(Throwable cause) {
        return cause;
      }
    };
  }

  @Override
  public Future<Map<Key, Entity>> get(Iterable<Key> keys) {
    GetOrCreateTransactionResult result = getOrCreateTransaction();
    return get(result.getTransaction(), keys);
  }

  @Override
  public Future<Map<Key, Entity>> get(Transaction txn, Iterable<Key> keys) {
    if (keys == null) {
      throw new NullPointerException("keys cannot be null");
    }

    List<Key> keyList = Lists.newArrayList(keys);

    Map<Key, Entity> resultMap = new HashMap<Key, Entity>();
    PreGetContext preGetContext = new PreGetContext(this, keyList, resultMap);
    datastoreServiceConfig.getDatastoreCallbacks().executePreGetCallbacks(preGetContext);

    keyList.removeAll(resultMap.keySet());

    PreGetCachingResult preGetCachingResult =
        entityCachingStrategy.preGet(this, keyList, resultMap);
    keyList.removeAll(preGetCachingResult.getKeysToSkipLoading());

    Future<Map<Key, Entity>> result = doBatchGet(txn, Sets.newLinkedHashSet(keyList), resultMap);

    result = entityCachingStrategy.createPostGetFuture(result, preGetCachingResult);
    return new PostLoadFuture(result, datastoreServiceConfig.getDatastoreCallbacks(), this);
  }

  private Future<Map<Key, Entity>> doBatchGet( Transaction txn, final Set<Key> keysToGet, final Map<Key, Entity> resultMap) {
    final GetRequest baseReq = new GetRequest();
    baseReq.setAllowDeferred(true);
    if (txn != null) {
      TransactionImpl.ensureTxnActive(txn);
      baseReq.setTransaction(localTxnToRemoteTxn(txn));
    }
    if (datastoreServiceConfig.getReadPolicy().getConsistency() == EVENTUAL) {
      baseReq.setFailoverMs(ARBITRARY_FAILOVER_READ_MS);
      baseReq.setStrong(false);
    }

    final boolean shouldUseMultipleBatches = getDatastoreType() != MASTER_SLAVE && txn == null
        && datastoreServiceConfig.getReadPolicy().getConsistency() != EVENTUAL;

    Iterator<GetRequest> batches =
        getByKeyBatcher.getBatches(keysToGet, baseReq, shouldUseMultipleBatches);
    List<Future<GetResponse>> futures = getByKeyBatcher.makeCalls(batches);

    return registerInTransaction(txn, new MultiFuture<GetResponse,  Map<Key, Entity>>(futures) {
      /**
       * A Map from a Reference without an App Id specified to the corresponding Key that the user
       * requested.  This is a workaround for the Remote API to support matching requested Keys to
       * Entities that may be from a different App Id .
       */
      private Map<Reference, Key> keyMapIgnoringAppId;

      @Override
      public Map<Key, Entity> get() throws InterruptedException, ExecutionException {
        try {
          aggregate(futures, null, null);
        } catch (TimeoutException e) {
          throw new RuntimeException(e);
        }
        return resultMap;
      }

      @Override
      public Map<Key, Entity> get(long timeout, TimeUnit unit)
          throws InterruptedException, ExecutionException, TimeoutException {
        aggregate(futures, timeout, unit);
        return resultMap;
      }

      /**
       * Aggregates the results of the given Futures and issues (synchronous) followup requests if
       * any results were deferred.
       *
       * @param currentFutures the Futures corresponding to the batches of the initial GetRequests.
       * @param timeout the timeout to use while waiting on the Future, or null for none.
       * @param timeoutUnit the unit of the timeout, or null for none.
       */
      private void aggregate(
          Iterable<Future<GetResponse>> currentFutures, Long timeout, TimeUnit timeoutUnit)
          throws ExecutionException, InterruptedException, TimeoutException {
        while (true) {
          List<Reference> deferredRefs = Lists.newLinkedList();

          for (Future<GetResponse> currentFuture : currentFutures) {
            GetResponse resp = getFutureWithOptionalTimeout(currentFuture, timeout, timeoutUnit);
            addEntitiesToResultMap(resp);
            deferredRefs.addAll(resp.deferreds());
          }

          if (deferredRefs.isEmpty()) {
            break;
          }

          Iterator<GetRequest> followupBatches =
              getByReferenceBatcher.getBatches(deferredRefs, baseReq, shouldUseMultipleBatches);
          currentFutures = getByReferenceBatcher.makeCalls(followupBatches);
        };
      }

      /**
       * Convenience method to get the result of a Future and optionally specify a timeout.
       *
       * @param future the Future to get.
       * @param timeout the timeout to use while waiting on the Future, or null for none.
       * @param timeoutUnit the unit of the timeout, or null for none.
       * @return the result of the Future.
       * @throws TimeoutException will only ever be thrown if a timeout is provided.
       */
      private GetResponse getFutureWithOptionalTimeout(
          Future<GetResponse> future, Long timeout, TimeUnit timeoutUnit)
          throws ExecutionException, InterruptedException, TimeoutException {
        if (timeout == null) {
          return future.get();
        } else {
          return future.get(timeout, timeoutUnit);
        }
      }

      /**
       * Adds the Entities from the GetResponse to the resultMap.  Will omit Keys that were missing.
       * Handles Keys with different App Ids from the Entity.Key.  See
       * {@link #findKeyFromRequestIgnoringAppId(Reference)}
       */
      private void addEntitiesToResultMap(GetResponse response) {
        for (GetResponse.Entity entityResult : response.entitys()) {
          if (entityResult.hasEntity()) {
            Entity responseEntity = EntityTranslator.createFromPb(entityResult.getEntity());
            Key responseKey = responseEntity.getKey();

            if (!keysToGet.contains(responseKey)) {
              responseKey = findKeyFromRequestIgnoringAppId(entityResult.getEntity().getKey());
            }
            resultMap.put(responseKey, responseEntity);
          }
        }
      }

      /**
       * This is a hack to support calls going through the Remote API.  The problem is:
       *
       * The requested Key may have a local app id.
       * The returned Entity may have a remote app id.
       *
       * In this case, we want to return a Map.Entry with {LocalKey, RemoteEntity}.  This way, the
       * user can always do map.get(keyFromRequest).
       *
       * This method will find the corresponding requested LocalKey for a RemoteKey by ignoring the
       * AppId field.
       *
       * Note that we used to be able to rely on the order of the Response Entities matching the
       * order of Request Keys.  We can no longer do so with the addition of Deferred results.
       *
       * @param referenceFromResponse the reference from the Response that did not match any of the
       *                              requested Keys.  (May be mutated.)
       * @return the Key from the request that corresponds to the given Reference from the Response
       *         (ignoring AppId.)
       */
      private Key findKeyFromRequestIgnoringAppId(Reference referenceFromResponse) {
        if (keyMapIgnoringAppId == null) {
          keyMapIgnoringAppId = Maps.newHashMap();
          for (Key requestKey : keysToGet) {
            Reference requestKeyAsRefWithoutApp = KeyTranslator.convertToPb(requestKey).clearApp();
            keyMapIgnoringAppId.put(requestKeyAsRefWithoutApp, requestKey);
          }
        }

        Key result = keyMapIgnoringAppId.get(referenceFromResponse.clearApp());
        if (result == null) {
          throw new DatastoreFailureException("Internal error");
        }
        return result;
      }
    });
  }

  @Override
  public Future<Key> put(Entity entity) {
    GetOrCreateTransactionResult result = getOrCreateTransaction();
    return put(result.getTransaction(), entity);
  }

  @Override
  public Future<Key> put(Transaction txn, Entity entity) {
    return new FutureWrapper<List<Key>, Key>(put(txn, Arrays.asList(entity))) {
      @Override
      protected Key wrap(List<Key> keys) throws Exception {
        return keys.get(0);
      }

      @Override
      protected Throwable convertException(Throwable cause) {
        return cause;
      }
    };
  }

  @Override
  public Future<List<Key>> put(Iterable<Entity> entities) {
    GetOrCreateTransactionResult result = getOrCreateTransaction();
    return put(result.getTransaction(), entities);
  }

  @Override
  public Future<List<Key>> put( Transaction txn, Iterable<Entity> entities) {
    List<Entity> entityList = entities instanceof List ?
        (List<Entity>) entities : Lists.newArrayList(entities);
    PutContext prePutContext = new PutContext(this, entityList);
    datastoreServiceConfig.getDatastoreCallbacks().executePrePutCallbacks(prePutContext);
    PreMutationCachingResult preMutationCachingResult =
        entityCachingStrategy.prePut(this, entityList);

    List<IndexedItem<Key>> indexedKeysToSkip = Lists.newArrayList();
    Set<Key> mutationKeysToSkip = preMutationCachingResult.getMutationKeysToSkip();
    List<Entity> entitiesToPut;
    if (!mutationKeysToSkip.isEmpty()) {
      entitiesToPut = Lists.newArrayList();
      int index = 0;
      for (Entity entity : entityList) {
        if (mutationKeysToSkip.contains(entity.getKey())) {
          indexedKeysToSkip.add(new IndexedItem<Key>(index++, entity.getKey()));
        } else {
          entitiesToPut.add(entity);
          ++index;
        }
      }
    } else {
      entitiesToPut = ImmutableList.copyOf(entities);
    }

    Future<List<Key>> result = combinePutResult(doBatchPut(txn, entitiesToPut), indexedKeysToSkip);

    if (txn == null) {
      result =
          entityCachingStrategy.createPostMutationFuture(result, preMutationCachingResult);
      PutContext postPutContext = new PutContext(this, entityList);
      result = new PostPutFuture(result, datastoreServiceConfig.getDatastoreCallbacks(),
          postPutContext);
    } else {
      defaultTxnProvider.addPutEntities(txn, entityList);
    }
    return result;
  }

  private Future<List<Key>> combinePutResult(Future<List<Key>> rpcResult,
      final List<IndexedItem<Key>> skippedKeys) {
    if (skippedKeys.isEmpty()) {
      return rpcResult;
    }

    return new FutureWrapper<List<Key>, List<Key>>(rpcResult) {
      @Override
      protected List<Key> wrap(List<Key> result) throws Exception {
        List<Key> combined = Lists.newLinkedList(result);
        for (IndexedItem<Key> indexedKey : skippedKeys) {
          combined.add(indexedKey.index, indexedKey.item);
        }
        return combined;
      }

      @Override
      protected Throwable convertException(Throwable cause) {
        return cause;
      }
    };
  }

  private Future<List<Key>> doBatchPut( Transaction txn,
      final List<Entity> entities) {
    PutRequest baseReq = new PutRequest();
    if (txn != null) {
      TransactionImpl.ensureTxnActive(txn);
      baseReq.setTransaction(localTxnToRemoteTxn(txn));
    }
    boolean group = !baseReq.hasTransaction();
    List<Integer> order = Lists.newArrayListWithCapacity(entities.size());
    Iterator<PutRequest> batches = putBatcher.getBatches(entities, baseReq, group, order);
    List<Future<PutResponse>> futures = putBatcher.makeCalls(batches);

    return registerInTransaction(txn,
        new ReorderingMultiFuture<PutResponse, List<Key>>(futures, order) {
          @Override
          protected List<Key> aggregate(
              PutResponse intermediateResult, Iterator<Integer> indexItr, List<Key> result) {
            for (Reference reference : intermediateResult.keys()) {
              int index = indexItr.next();
              Key key = entities.get(index).getKey();
              KeyTranslator.updateKey(reference, key);
              result.set(index, key);
            }
            return result;
          }

          @Override
          protected List<Key> initResult(int size) {
            List<Key> result = new ArrayList<Key>(Collections.<Key>nCopies(size, null));
            return result;
          }
        });
  }

  @Override
  public Future<Void> delete(Key... keys) {
    GetOrCreateTransactionResult result = getOrCreateTransaction();
    return delete(result.getTransaction(), keys);
  }

  @Override
  public Future<Void> delete(Transaction txn, Key... keys) {
    return delete(txn, Arrays.asList(keys));
  }

  @Override
  public Future<Void> delete(Iterable<Key> keys) {
    GetOrCreateTransactionResult result = getOrCreateTransaction();
    return delete(result.getTransaction(), keys);
  }

  @Override
  public Future<Void> delete(Transaction txn, Iterable<Key> keys) {
    List<Key> allKeys = keys instanceof List ?
        (List<Key>) keys : ImmutableList.copyOf(keys);
    DeleteContext preDeleteContext = new DeleteContext(this, allKeys);
    datastoreServiceConfig.getDatastoreCallbacks().executePreDeleteCallbacks(preDeleteContext);
    PreMutationCachingResult preMutationCachingResult =
        entityCachingStrategy.preDelete(this, allKeys);
    Future<Void> result = null;
    Collection<Key> keysToDelete;
    Set<Key> keysToSkip = preMutationCachingResult.getMutationKeysToSkip();
    if (keysToSkip.isEmpty()) {
      keysToDelete = allKeys;
    } else {
      Set<Key> keySet = Sets.newHashSet(allKeys);
      keySet.removeAll(keysToSkip);
      keysToDelete = keySet;
    }
    result = doBatchDelete(txn, keysToDelete);

    if (txn == null) {
      result = entityCachingStrategy.createPostMutationFuture(result, preMutationCachingResult);
      result = new PostDeleteFuture(
          result, datastoreServiceConfig.getDatastoreCallbacks(),
          new DeleteContext(this, allKeys));
    } else {
      defaultTxnProvider.addDeletedKeys(txn, allKeys);
    }
    return result;
  }

  private Future<Void> doBatchDelete( Transaction txn, Collection<Key> keys) {
    DeleteRequest baseReq = new DeleteRequest();
    if (txn != null) {
      TransactionImpl.ensureTxnActive(txn);
      baseReq.setTransaction(localTxnToRemoteTxn(txn));
    }
    boolean group = !baseReq.hasTransaction();
    Iterator<DeleteRequest> batches = deleteBatcher.getBatches(keys, baseReq, group);
    List<Future<DeleteResponse>> futures = deleteBatcher.makeCalls(batches);
    return registerInTransaction(txn, new MultiFuture<DeleteResponse, Void>(futures) {
      @Override
      public Void get() throws InterruptedException, ExecutionException {
        for (Future<DeleteResponse> future : futures) {
          future.get();
        }
        return null;
      }

      @Override
      public Void get(long timeout, TimeUnit unit)
          throws InterruptedException, ExecutionException, TimeoutException {
        for (Future<DeleteResponse> future : futures) {
          future.get(timeout, unit);
        }
        return null;
      }
    });
  }

  @Override
  public Collection<Transaction> getActiveTransactions() {
    return defaultTxnProvider.getAll();
  }

  /**
   * Register the provided future with the provided txn so that we know to
   * perform a {@link java.util.concurrent.Future#get()} before the txn is
   * committed.
   *
   * @param txn The txn with which the future must be associated.
   * @param future The future to associate with the txn.
   * @param <T> The type of the Future
   * @return The same future that was passed in, for caller convenience.
   */
  private <T> Future<T> registerInTransaction( Transaction txn, Future<T> future) {
    if (txn != null) {
      defaultTxnProvider.addFuture(txn, future);
      return new FutureHelper.TxnAwareFuture<T>(future, txn, defaultTxnProvider);
    }
    return future;
  }

  @Override
  public Future<Transaction> beginTransaction() {
    return beginTransaction(TransactionOptions.Builder.withDefaults());
  }

  @Override
  public Future<Transaction> beginTransaction(TransactionOptions options) {
    return new FutureHelper.FakeFuture<Transaction>(beginTransactionInternal(options));
  }

  @Override
  public PreparedQuery prepare(Query query) {
    return prepare(null, query);
  }

  @SuppressWarnings("deprecation")
  @Override
  public PreparedQuery prepare(Transaction txn, Query query) {
    PreQueryContext context = new PreQueryContext(this, query);
    datastoreServiceConfig.getDatastoreCallbacks().executePreQueryCallbacks(context);

    query = context.getElements().get(0);
    validateQuery(query);
    List<MultiQueryBuilder> queriesToRun = QuerySplitHelper.splitQuery(query);
    query.setFilter(null);
    query.getFilterPredicates().clear();
    if (queriesToRun.size() == 1 && queriesToRun.get(0).isSingleton()) {
      query.getFilterPredicates().addAll(queriesToRun.get(0).getBaseFilters());
      return new PreparedQueryImpl(apiConfig, datastoreServiceConfig, query, txn);
    }
    return new PreparedMultiQuery(apiConfig, datastoreServiceConfig, query, queriesToRun, txn);
  }

  @Override
  public Future<KeyRange> allocateIds(String kind, long num) {
    return allocateIds(null, kind, num);
  }

  static Reference buildAllocateIdsRef(
      Key parent, String kind, AppIdNamespace appIdNamespace) {
    if (parent != null && !parent.isComplete()) {
      throw new IllegalArgumentException("parent key must be complete");
    }
    Key key = new Key(kind, parent, Key.NOT_ASSIGNED, "ignored", appIdNamespace);
    return KeyTranslator.convertToPb(key);
  }

  @Override
  public Future<KeyRange> allocateIds(final Key parent, final String kind, long num) {
    if (num <= 0) {
      throw new IllegalArgumentException("num must be > 0");
    }

    if (num > 1000000000) {
      throw new IllegalArgumentException("num must be < 1 billion");
    }

    final AppIdNamespace appIdNamespace = datastoreServiceConfig.getAppIdNamespace();
    Reference allocateIdsRef = buildAllocateIdsRef(parent, kind, appIdNamespace);
    AllocateIdsRequest req =
        new AllocateIdsRequest().setSize(num).setModelKey(allocateIdsRef);
    AllocateIdsResponse resp = new AllocateIdsResponse();
    Future<AllocateIdsResponse> future = makeAsyncCall(apiConfig, "AllocateIds", req, resp);
    return new FutureWrapper<AllocateIdsResponse, KeyRange>(future) {
      @Override
      protected KeyRange wrap(AllocateIdsResponse resp) throws Exception {
        return new KeyRange(parent, kind, resp.getStart(), resp.getEnd(), appIdNamespace);
      }

      @Override
      protected Throwable convertException(Throwable cause) {
        return cause;
      }
    };
  }

  protected DatastoreType getDatastoreType() {
    if (datastoreType == null) {
      datastoreType = quietGet(getDatastoreAttributes()).getDatastoreType();
    }
    return datastoreType;
  }

  @Override
  public Future<DatastoreAttributes> getDatastoreAttributes() {
    String appId = datastoreServiceConfig.getAppIdNamespace().getAppId();
    DatastoreAttributes attributes = new DatastoreAttributes(appId);
    return new FutureHelper.FakeFuture<DatastoreAttributes>(attributes);
  }

  @Override
  public Future<Map<Index, IndexState>> getIndexes() {
    StringProto req = new StringProto();
    req.setValue(datastoreServiceConfig.getAppIdNamespace().getAppId());
    return new FutureWrapper<CompositeIndices, Map<Index, IndexState>>(
        makeAsyncCall(apiConfig, "GetIndices", req, new CompositeIndices())) {
      @Override
      protected Map<Index, IndexState> wrap(CompositeIndices indices) throws Exception {
        Map<Index, IndexState> answer = new LinkedHashMap<Index, IndexState>();
        for (CompositeIndex ci : indices.indexs()) {
          Index index = IndexTranslator.convertFromPb(ci);
          switch (ci.getStateEnum()) {
            case DELETED:
              answer.put(index, IndexState.DELETING);
              break;
            case ERROR:
              answer.put(index, IndexState.ERROR);
              break;
            case READ_WRITE:
              answer.put(index, IndexState.SERVING);
              break;
            case WRITE_ONLY:
              answer.put(index, IndexState.BUILDING);
              break;
            default:
              logger.log(Level.WARNING, "Unrecognized index state for " + index);
              break;
          }
        }
        return answer;
      }

      @Override
      protected Throwable convertException(Throwable cause) {
        return cause;
      }
    };
  }
}
